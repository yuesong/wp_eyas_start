<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
	register_setting( 'es_options', 'eyas_start_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
	add_options_page( __( 'Eyas Start', 'ey' ), __( 'Eyas Start', 'ey' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}




/**
 * Create the options page
 */
function theme_options_do_page() {
	// 初始化设置
	$default_options = array(
		'google_font' => 'disabled',
		'admin_footer_text' => '网站管理系统'
		);
	// 所有设置
	$ey_options = array(
		'google_font' => array(
			'disabled' => __('disabled','ey'),
			'enabled'  => __('enabled','ey'),
			'360_font' => __('360 CDN','ey')
			),
		'admin_top_logo' => array(
			'wp_logo'	=> __('wordpress logo','ey'),
			'site_name'	=> __('Site name','ey'),
			'updates'	=> __('updates icon','ey'),
			'comments'	=> __('comments icon','ey'),
			'new-content'	=> __('New content','ey'),
			),
		'disable_head_code'	=> array(
			'rsd_link' => __('RSD Link','ey'),
			'wlwmanifest_link' => __('wlwmanifest link','ey'),
			'wp_generator' => __('wp generator','ey'),
			'start_post_rel_link' => __('start post rel link','ey'),
			'index_rel_link' => __('index rel link','ey'),
			'adjacent_posts_rel_link' => __('adjacent posts rel link','ey')
			),
		'admin_footer_text' => __('Wordpress CMS','ey'),
		'fix_upload_image' => __('Enabled','ey'),
		'xmlrpc_enabled' => __('Disabled','ey'),
		'save_setting'	=> array(
			'load_local_lang' => __('Do not load local language package','ey'),
			'limit_url_size' => __('Limit URL size','ey'),
			'respond_304'	=> __('304 not modify','ey')
			),
		'smtp_email'	=> array(
			'enabled'	=> __('Enabled SMTP Email','ey'),
			'labels'	=> array(
				'FromName'  => __('Form Name','ey'),
				'host'		=> __('Host','ey'),
				'port'		=> __('Port','ey'),
				'username'	=> __('Username','ey'),
				'password'	=> __('Password','ey'),
				'form'		=> __('Form','ey')
				),
			'smtp_ssl'	=> __('Enabled SSL','ey')
			),
		'function_enabled' => __('启用 Eyas Start 插件函数，函数文档见<a href="http://eyaslife.com/post/439">这里</a>')

		);
	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false;
	?>
	<div class="wrap">
		<?php screen_icon(); echo "<h2>" . __( ' Eyas Start', 'ey' ) . "</h2>"; ?>

		<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
		<?php endif; ?>

		<form method="post" action="options.php">
			<?php settings_fields( 'es_options' ); ?>
			<?php $options = get_option( 'eyas_start_options' ); ?>

			<table class="form-table">
				<?php /*google 字体*/ ?>
				<tr valign="top"><th scope="row"><?php _e( 'Google Font', 'ey' ); ?></th>
					<td>
						<fieldset><legend class="screen-reader-text"><span><?php _e( 'Google Font', 'ey' ); ?></span></legend>
						<?php foreach($ey_options['google_font'] as $value=>$label){ 
							$checked = "";
							if($options['google_font'] == $value){
								$checked = "checked";
							}
						 ?>
							<label class="description"><input type="radio" <?php echo $checked; ?> name="eyas_start_options[google_font]" value="<?php esc_attr_e( $value ); ?>"  /><?php echo $label; ?></label><br />
						<?php } ?>
						</fieldset>
					</td>
				</tr>

				<?php
				/**
				 * 后台底部版权信息
				 */
				?>
				<tr valign="top"><th scope="row"><?php _e( 'Admin footer text', 'ey' ); ?></th>
					<td>
						<input class="eyas_start_options[admin_footer_text]" class="regular-text" type="text" name="eyas_start_options[admin_footer_text]" value="<?php esc_attr_e( $options['admin_footer_text'] ); ?>" />
						<label class="description" for="eyas_start_options[admin_footer_text]"><?php _e( 'Display in administrator page gooter', 'ey' ); ?></label>
					</td>
				</tr>
				<?php /*移除顶部logo*/ ?>
				<tr>
					<th scope="row">移除wordpress顶部logo</th>
					<td><fieldset><legend class="screen-reader-text"><span>移除wordpress顶部logo</span></legend>
						<?php foreach($ey_options['admin_top_logo'] as $value=>$label){ 
							$checked = "";
							if($options['admin_top_logo'][$value] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="<?php echo $value; ?>"><input name="eyas_start_options[admin_top_logo][<?php echo $value; ?>]" type="checkbox" class="<?php echo $value; ?>" <?php echo $checked; ?>><?php echo $label; ?></label><br>
						<?php } ?>
					</fieldset></td>
				</tr>
				<?php /*移除顶部logo*/ ?>
				<tr>
					<th scope="row">修复中文名图片上传</th>
					<td><fieldset><legend class="screen-reader-text"><span>修复中文名图片上传</span></legend>
						<?php $checked = "";
							if($options['fix_upload_image'] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="fix_upload_image"><input name="eyas_start_options[fix_upload_image]" type="checkbox" class="eyas_start_options[fix_upload_image]" <?php echo $checked; ?>><?php echo $ey_options['fix_upload_image']; ?></label><br>
						
					</fieldset></td>
				</tr>

				<?php /*去除头部无用代码*/ ?>
				<tr>
					<th scope="row">去除头部无用代码</th>
					<td><fieldset><legend class="screen-reader-text"><span>去除头部无用代码</span></legend>
						<?php foreach($ey_options['disable_head_code'] as $value=>$label){ 
							$checked = "";
							if($options['disable_head_code'][$value] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="<?php echo $value; ?>"><input name="eyas_start_options[disable_head_code][<?php echo $value; ?>]" type="checkbox" class="<?php echo $value; ?>" <?php echo $checked; ?>><?php echo $label; ?></label><br>
						<?php } ?>
					</fieldset></td>
				</tr>

				<?php /*移除顶部logo*/ ?>
				<tr>
					<th scope="row">关闭XMLRPC</th>
					<td><fieldset><legend class="screen-reader-text"><span>关闭XMLRPC</span></legend>
						<?php $checked = "";
							if($options['xmlrpc_enabled'] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="xmlrpc_enabled"><input name="eyas_start_options[xmlrpc_enabled]" type="checkbox" class="eyas_start_options[xmlrpc_enabled]" <?php echo $checked; ?>><?php echo $ey_options['xmlrpc_enabled']; ?></label><br>
						
					</fieldset></td>
				</tr>

				<?php /*SMTP 邮箱*/ ?>
				<tr>
					<th scope="row">SMTP 邮箱设置</th>
					<td><fieldset><legend class="screen-reader-text"><span>SMTP 邮箱设置</span></legend>
						<?php $checked = "";
							if($options['smtp_email']['enabled'] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="smtp_email"><input name="eyas_start_options[smtp_email][enabled]" type="checkbox" class="eyas_start_options-smtp_email-enabled" <?php echo $options['smtp_email']['enabled']=='on' ? 'checked' :''; ?>><?php echo $ey_options['smtp_email']['enabled']; ?></label><br>
						<label for="smtp_email"><input name="eyas_start_options[smtp_email][smtp_ssl]" type="checkbox" class="eyas_start_options-smtp_email" <?php echo $options['smtp_email']['smtp_ssl']=='on' ? 'checked' :''; ?>><?php echo $ey_options['smtp_email']['smtp_ssl']; ?></label><br>
					</fieldset></td>
				</tr>
<?php foreach($ey_options['smtp_email']['labels'] as $value=>$label): ?>
				<tr class="smtp_email_labels" valign="top"><th scope="row" style="text-align: right;"><?php echo $label; ?></th>
					<td>
						<input name="eyas_start_options[smtp_email][<?php echo $value; ?>]" type="text" class="eyas_start_options-smtp_email-<?php echo $value; ?>">
					</td>
				</tr>
<?php endforeach; ?>
				<?php /*移除顶部logo*/ ?>
				<tr>
					<th scope="row">启用 Eyas Start 插件函数</th>
					<td><fieldset><legend class="screen-reader-text"><span>启用 Eyas Start 插件函数</span></legend>
						<?php $checked = "";
							if($options['function_enabled'] == 'on'){
								$checked = 'checked';
							}
						 ?>
						<label for="function_enabled"><input name="eyas_start_options[function_enabled]" type="checkbox" class="eyas_start_options[function_enabled]" <?php echo $checked; ?>><?php echo $ey_options['function_enabled']; ?></label><br>
						
					</fieldset></td>
				</tr>
			</table>
			<p class="submit">
				<input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'ey' ); ?>" />
			</p>
		</form>

	</div>
<script>
	jQuery(document).ready(function($){
		$('.eyas_start_options-smtp_email-password').attr('type','password');
		if(!$('.eyas_start_options-smtp_email-enabled').is(':checked')){
			$('.smtp_email_labels').css('display','none');
			$('.eyas_start_options-smtp_email').attr('disabled',true);
		}else{
			$('.smtp_email_labels').css('display','table-row');
			$('.eyas_start_options-smtp_email').attr('disabled',false);
		}
		$('body').on('change','.eyas_start_options-smtp_email-enabled',function(e){
			if(!$('.eyas_start_options-smtp_email-enabled').is(':checked')){
			$('.smtp_email_labels').css('display','none');
			$('.eyas_start_options-smtp_email').attr('disabled',true);
		}else{
			$('.smtp_email_labels').css('display','table-row');
			$('.eyas_start_options-smtp_email').attr('disabled',false);
		}
		})
	})
</script>
	<?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function theme_options_validate( $input ) {
	global $select_options, $radio_options,$google_font;
	var_dump($input);
	return $input;
}

// adapted from http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register      _setting/