<?php
/**
 * Eyas start
 *
 * @package   Eyas start
 * @author    Eyas <liuyuesongde@163.com>
 * @license   GPL-2.0+
 * @link      http://eyaslife.com
 * @copyright 2010-2014 WP Theming
 *
 * @wordpress-plugin
 * Plugin Name: wp Eyas Start
 * Plugin URI:  http://eyaslife.com
 * Description: 一个用于主题快速开发的插件.
 * Version:     1.0
 * Author:      Eyas
 * Author URI:  http://eyaslife.com
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: ey
 * Domain Path: /lang
 */

// 插件目录
define('PLUGIN_DIR',plugin_dir_path( __FILE__ ));


function wp_eyas_start_init() {

	// 加载翻译文件
	load_plugin_textdomain( 'ey', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

	// 加载插件
	require PLUGIN_DIR . '/inc/plugin_init.php';
}
add_action( 'init', 'wp_eyas_start_init', 20 );
// 插件选项
require PLUGIN_DIR . '/options/options.php';