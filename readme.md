## 初衷

* * *

每一次使用wordpress做项目的时候，都要重复一大堆大堆的工作，有一天，我实在受够了这种重复的工作，决定要把那些经常用的东西给整理下来，等下次做项目的时候，这些重复的工作可以完全的给省略掉它。所以，我给自己编写了一个wordpress主题开发的框架，这个框架我给他命名为：Eyas start。主要面向的人群是主题开发的开发者，而不是用户。

## 使用方法

首先下载好插件，上传至插件目录 `/wp-content/plugins/`，并启用。然后到 `设置/Eyas Start`页面配置好插件。插件目录有一个`eyas_start`文件夹，这是一个初始化主题，只创建了最最基本的文件，可以复制该主题到主题目录开始主题开发

## 关于函数名

Eyas start定义的函数，全部都是用`ey_`作为前缀，为什么要前缀，是因为命名空间问题。要起一个富有语义化的函数名，稍微不好就跟官方函数冲突了，所以不得已加了个前缀。如果你不喜欢，可以打开 `/inc/plugins/function.php`文件把里面的前缀给改了，至于其他文件，改不改前缀都一样，反正你在主题开发过程中用不到。

## 特别鸣谢

本插件的很多功能，其实有一部分是从网上摘抄下来的，不过原创的函数也是不少的。在`我爱水煮鱼`的网站摘抄的应该最多了，因为那里很多实用的功能，然后经过我的修改，优化一下，让大家使用的更加的方便。还有其他朋友，不好意思，没能把你们的名字和博客名给记住，因为太散了，而且一段代码在好几个网站上都是一摸一样的，所以我也不知道原创者是谁，所以就没办法列举了。

## 建议与意见

如果对本插件有什么建议或者意见的话，可以加我`qq893521870`，或者联系我邮箱`liuyuesongde@163.com`,或者评论本文章，或者到`git` 添加`issue` 。

## 下载地址

git@osc:http://git.oschina.net/yuesong/eyas_start

博客地址：http://eyaslife.com

## 使用文档
   * [Eyas start 主题框架——概况](http://eyaslife.com/post/433)
   * [Eyas start 主题框架——函数](http://eyaslife.com/post/439)
   * [Eyas start 主题框架——优化设置](http://eyaslife.com/post/452)