<?php 
/*
Plugin Name: Smtp email
Plugin URI: https://eyaslife.com
Description: 开启smtp发送email
Usage: 
Version: 1.2
Author: Pedro Eyas
Author URI: https://eyaslife.com
*/
//使用smtp发送邮件，这里是QQ邮箱，你可以参照你使用的邮箱具体设置SMTP
$options = get_option( 'eyas_start_options' );
if(!function_exists('ey_mail_setup')):
function ey_mail_setup( $phpmailer ) {
	$options = get_option( 'eyas_start_options' );
	$phpmailer->FromName = $options['smtp_email']['FromName']; //发件人
	$phpmailer->Host = $options['smtp_email']['Host']; //修改为你使用的SMTP服务器
	$phpmailer->Port = $options['smtp_email']['Port']; //SMTP端口
	$phpmailer->Username = $options['smtp_email']['Username']; //邮箱账户   
	$phpmailer->Password = $options['smtp_email']['Password']; //邮箱密码
	$phpmailer->From = $options['smtp_email']['From']; //你的邮箱   
	$phpmailer->SMTPAuth = true;   
	$phpmailer->SMTPSecure = ($options['smtp_email']['smtp_ssl']=='on')?'ssl':'tsl'; //tls or ssl （port=25留空，465为ssl）
	$phpmailer->IsSMTP();
}
if($options['smtp_email'] == 'on'){
	add_action('phpmailer_init', 'mail_smtp');
}
endif;