<?php 
$options = get_option( 'eyas_start_options' );
// 禁用google字体，优化后台速度
if(!function_exists('ey_disable_googlefonts')):
function ey_disable_googlefonts( $translations, $text, $context, $domain ) {
    if ( 'Open Sans font: on or off' == $context && 'on' == $text ) {
    	$translations = 'off';
	}
	return $translations;
}

function ey_replace_360font_open_sans() {
	wp_deregister_style('open-sans');
	wp_register_style( 'open-sans', '//fonts.useso.com/css?family=Open+Sans:300italic,400italic,600italic,300,400,600' );
	wp_enqueue_style( 'open-sans');
}
if($options['google_font'] == 'disabled'){
	add_filter( 'gettext_with_context', 'ey_disable_googlefonts', 888, 4 );
}elseif($options['google_font'] == '360_font'){
	add_action( 'wp_enqueue_scripts', 'ey_replace_360font_open_sans' );
	add_action('admin_enqueue_scripts', 'ey_replace_360font_open_sans');
}else{}
endif;

// 修改wp后台底部版权信息
if(!function_exists('ey_modify_footer_admin')):
function ey_modify_footer_admin () {
	echo $options["admin_footer_text"];
}
add_filter('admin_footer_text', 'ey_modify_footer_admin');
endif;

// 移除顶部logo
if(!function_exists('ey_admin_bar_remove_logo')):
function ey_admin_bar_remove_logo() {
	global $wp_admin_bar;
	$options = get_option( 'eyas_start_options' );
	if($options['admin_top_logo']['wp_logo']=='on') $wp_admin_bar->remove_menu('wp-logo');
	if($options['admin_top_logo']['comments']=='on') $wp_admin_bar->remove_menu('comments');
	if($options['admin_top_logo']['updates']=='on') $wp_admin_bar->remove_menu('updates');
	if($options['admin_top_logo']['site_name']=='on') $wp_admin_bar->remove_menu('site-name');
	if($options['admin_top_logo']['new-content']=='on') $wp_admin_bar->remove_menu('new-content');
}
add_action('wp_before_admin_bar_render', 'ey_admin_bar_remove_logo', 0);
endif;

// 中文名图片上传
if(!function_exists('ey_upload_file_chinese_name')):
function ey_upload_file_chinese_name($filename) {  
	$parts = explode('.', $filename);  
	$filename = array_shift($parts);  
	$extension = array_pop($parts);  
	foreach ( (array) $parts as $part)  
	$filename .= '.' . $part;  
	    
	if(preg_match('/[一-龥]/u', $filename)){  
		$filename = md5($filename);  
	}  
	$filename .= '.' . $extension;  
	return $filename ;  
}  
if($options['fix_upload_image'] == 'on'){
	add_filter('sanitize_file_name', 'ey_upload_file_chinese_name', 5,1);
}
endif;

//去除头部无用代码
if($options['disable_head_code']['rsd_link']=='on') remove_action('wp_head', 'rsd_link');
if($options['disable_head_code']['wlwmanifest_link']=='on') remove_action('wp_head', 'wlwmanifest_link');
if($options['disable_head_code']['wp_generator']=='on') remove_action('wp_head', 'wp_generator');
if($options['disable_head_code']['start_post_rel_link']=='on') remove_action('wp_head', 'start_post_rel_link');
if($options['disable_head_code']['index_rel_link']=='on') remove_action('wp_head', 'index_rel_link');
if($options['disable_head_code']['adjacent_posts_rel_link']=='on') remove_action('wp_head', 'adjacent_posts_rel_link'); 
// 禁用embed
remove_filter( 'the_content', array( $GLOBALS['wp_embed'], 'autoembed' ), 8 );

/*
 * 关闭XMLRPC
 */
if($options['xmlrpc_enabled']=='on')  add_filter('xmlrpc_enabled', '__return_false');


