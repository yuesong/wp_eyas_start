<?php 
/*
 * options framework 设置框架
 */
// require PLUGIN_DIR . '/inc/plugins/options-framework/options-framework.php';

/*
 *修改默认设置
 */
require PLUGIN_DIR . '/inc/plugins/settings.php';

/*
 * 面包屑
 */
require PLUGIN_DIR . '/inc/plugins/full-breadcrumb.php';

/*
 * 更改wordpress一些安全设置
 */
require PLUGIN_DIR . '/inc/plugins/save.php';

/*
 * 开启smtp发送email
 */
require PLUGIN_DIR . '/inc/plugins/email.php';

/*
 * 开启304 返回，提高网站效率,开发阶段不建议开启
 */
require PLUGIN_DIR . '/inc/plugins/304-respond.php';

/*
 * 缓存菜单
 */
require PLUGIN_DIR . '/inc/plugins/wp-nav-menu-cache.php';

/*
 * 定义常用函数
 */
if(get_option( 'eyas_start_options' )['function_enabled'] == 'on'){
require PLUGIN_DIR . '/inc/plugins/function.php';
}